package com.soprabanking.analytics.ds501;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ds501Application {

    public static void main(String[] args) {
        SpringApplication.run(Ds501Application.class, args);
    }
}
