package com.soprabanking.analytics.ds501.models;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ds501")
public class IntfDS501 {
    @Id private String id;

    private String msgheadertype;
    private String cpycod;
    private String busdate;
    private String msgversionnb;
    private String appcod;
    private String exchgmessagetype;
    private String eventtyp;
    private String eventlinenb;
    private String accountingruleappldt;
    private String accountingbalancechck;
    private String accountingreversalcd;
    private String acctidtyp;
    private String acctid;
    private String dpstsvgproductctgy;
    private int dpstsvgbkproduct;
    private String managingentity;
    private String cancellationstatus;
    private Double acctbalancemnt;
    private Double acctpreviousbalancemnt;
    private String acctcur;
    private String acctbalancedt;
    private String acctpreviousbalancedt;
    private String currentmonthoverdraftdaycnt;
    private Double accttotdaydbtmnt;
    private Double accttotdaycrdmnt;
    private Double currentdaydbtmovementcnt;
    private Double currentdaycrdmovementcnt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMsgheadertype() {
        return msgheadertype;
    }

    public void setMsgheadertype(String msgheadertype) {
        this.msgheadertype = msgheadertype;
    }

    public String getCpycod() {
        return cpycod;
    }

    public void setCpycod(String cpycod) {
        this.cpycod = cpycod;
    }

    public String getBusdate() {
        return busdate;
    }

    public void setBusdate(String busdate) {
        this.busdate = busdate;
    }

    public String getMsgversionnb() {
        return msgversionnb;
    }

    public void setMsgversionnb(String msgversionnb) {
        this.msgversionnb = msgversionnb;
    }

    public String getAppcod() {
        return appcod;
    }

    public void setAppcod(String appcod) {
        this.appcod = appcod;
    }

    public String getExchgmessagetype() {
        return exchgmessagetype;
    }

    public void setExchgmessagetype(String exchgmessagetype) {
        this.exchgmessagetype = exchgmessagetype;
    }

    public String getEventtyp() {
        return eventtyp;
    }

    public void setEventtyp(String eventtyp) {
        this.eventtyp = eventtyp;
    }

    public String getEventlinenb() {
        return eventlinenb;
    }

    public void setEventlinenb(String eventlinenb) {
        this.eventlinenb = eventlinenb;
    }

    public String getAccountingruleappldt() {
        return accountingruleappldt;
    }

    public void setAccountingruleappldt(String accountingruleappldt) {
        this.accountingruleappldt = accountingruleappldt;
    }

    public String getAccountingbalancechck() {
        return accountingbalancechck;
    }

    public void setAccountingbalancechck(String accountingbalancechck) {
        this.accountingbalancechck = accountingbalancechck;
    }

    public String getAccountingreversalcd() {
        return accountingreversalcd;
    }

    public void setAccountingreversalcd(String accountingreversalcd) {
        this.accountingreversalcd = accountingreversalcd;
    }

    public String getAcctidtyp() {
        return acctidtyp;
    }

    public void setAcctidtyp(String acctidtyp) {
        this.acctidtyp = acctidtyp;
    }

    public String getAcctid() {
        return acctid;
    }

    public void setAcctid(String acctid) {
        this.acctid = acctid;
    }

    public String getDpstsvgproductctgy() {
        return dpstsvgproductctgy;
    }

    public void setDpstsvgproductctgy(String dpstsvgproductctgy) {
        this.dpstsvgproductctgy = dpstsvgproductctgy;
    }

    public int getDpstsvgbkproduct() {
        return dpstsvgbkproduct;
    }

    public void setDpstsvgbkproduct(int dpstsvgbkproduct) {
        this.dpstsvgbkproduct = dpstsvgbkproduct;
    }

    public String getManagingentity() {
        return managingentity;
    }

    public void setManagingentity(String managingentity) {
        this.managingentity = managingentity;
    }

    public String getCancellationstatus() {
        return cancellationstatus;
    }

    public void setCancellationstatus(String cancellationstatus) {
        this.cancellationstatus = cancellationstatus;
    }

    public Double getAcctbalancemnt() {
        return acctbalancemnt;
    }

    public void setAcctbalancemnt(Double acctbalancemnt) {
        this.acctbalancemnt = acctbalancemnt;
    }

    public Double getAcctpreviousbalancemnt() {
        return acctpreviousbalancemnt;
    }

    public void setAcctpreviousbalancemnt(Double acctpreviousbalancemnt) {
        this.acctpreviousbalancemnt = acctpreviousbalancemnt;
    }

    public String getAcctcur() {
        return acctcur;
    }

    public void setAcctcur(String acctcur) {
        this.acctcur = acctcur;
    }

    public String getAcctbalancedt() {
        return acctbalancedt;
    }

    public void setAcctbalancedt(String acctbalancedt) {
        this.acctbalancedt = acctbalancedt;
    }

    public String getAcctpreviousbalancedt() {
        return acctpreviousbalancedt;
    }

    public void setAcctpreviousbalancedt(String acctpreviousbalancedt) {
        this.acctpreviousbalancedt = acctpreviousbalancedt;
    }

    public String getCurrentmonthoverdraftdaycnt() {
        return currentmonthoverdraftdaycnt;
    }

    public void setCurrentmonthoverdraftdaycnt(String currentmonthoverdraftdaycnt) {
        this.currentmonthoverdraftdaycnt = currentmonthoverdraftdaycnt;
    }

    public Double getAccttotdaydbtmnt() {
        return accttotdaydbtmnt;
    }

    public void setAccttotdaydbtmnt(Double accttotdaydbtmnt) {
        this.accttotdaydbtmnt = accttotdaydbtmnt;
    }

    public Double getAccttotdaycrdmnt() {
        return accttotdaycrdmnt;
    }

    public void setAccttotdaycrdmnt(Double accttotdaycrdmnt) {
        this.accttotdaycrdmnt = accttotdaycrdmnt;
    }

    public Double getCurrentdaydbtmovementcnt() {
        return currentdaydbtmovementcnt;
    }

    public void setCurrentdaydbtmovementcnt(Double currentdaydbtmovementcnt) {
        this.currentdaydbtmovementcnt = currentdaydbtmovementcnt;
    }

    public Double getCurrentdaycrdmovementcnt() {
        return currentdaycrdmovementcnt;
    }

    public void setCurrentdaycrdmovementcnt(Double currentdaycrdmovementcnt) {
        this.currentdaycrdmovementcnt = currentdaycrdmovementcnt;
    }
}
