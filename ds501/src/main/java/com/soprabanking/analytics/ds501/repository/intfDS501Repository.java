package com.soprabanking.analytics.ds501.repository;

import java.util.List;
import com.soprabanking.analytics.ds501.models.IntfDS501;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "IntfDS501", path = "ds501")
public interface intfDS501Repository extends MongoRepository<IntfDS501, String> {

        List<IntfDS501> dpstsvgbkproduct(@Param("id") String dpstsvgbkproduct);
        List<IntfDS501> acctid(@Param("id") String acctid);



        }